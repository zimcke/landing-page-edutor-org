---
title: "Alexander Tang 鄧劍豪"
date: 2018-11-19T10:47:58+10:00
image: "images/team/alexander-tang.jpeg"
jobtitle: "Educational Consultant"
linkedinurl: "https://www.linkedin.com/in/alexander-gh-tang/"
promoted: true
weight: 2
---

Alexander is an **accomplished Senior Software Engineer with a solid experience from prominent Belgian companies**, including government-related entities such as Acerta and VDAB, where his expertise has predominantly centered on payroll management, workforce training and social contributions.



His diverse professional journey extends beyond software engineering, reflecting his dedication to education. Before embarking on his software engineering career, Alexander served as a **professional tutor, specializing in scientific subjects tailored for university students**. This included preparing students for the demanding entrance examinations for medical degrees in Belgium.



Alexander's academic credentials are equally impressive. He holds a Master's degree in **Computer Science Engineering, with a specialization in Artificial Intelligence**, granted by the prestigious University of Leuven. His educational background, coupled with his extensive professional experience, exemplify his commitment to expertise in his chosen field. Alexander is a valued member of our team at Edutor Consulting, contributing his profound knowledge and dedication to our mission of empowering international students in their pursuit of educational excellence in Belgium.