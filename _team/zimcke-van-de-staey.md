---
title: "Zimcke Van de Staey"
date: 2018-11-19T10:47:58+10:00
image: "images/team/zimcke-van-de-staey.jpeg"
jobtitle: "Director & Educational Consultant"
linkedinurl: "https://www.linkedin.com/in/zimcke-van-de-staey-41625474/"
promoted: true
weight: 1
---

Zimcke is the founder of Edutor Consulting. She grew up in Belgium and knows the ins and outs of Belgian universities, having studied humanities and computer science at both University of Leuven (KU Leuven) and Ghent University (UGent). She was enrolled in a fully-funded PhD programme at Ghent University, but in 2021 she moved to Hong Kong to start up BytePact Ltd. 