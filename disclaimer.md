---
title: Disclaimer
layout: page
description: Disclaimer
bodyClass: page-about
---

## 1. Interpretation and Definitions

### Interpretation
The words of which the initial letter is capitalized have meanings defined under the following conditions. The following definitions shall have the same meaning regardless of whether they appear in singular or plural.

### Definitions
For the purposes of this Disclaimer:

- **Company** (referred to as either "the Company", "We", "Us" or "Our" in this Disclaimer) refers to BytePact Ltd., 2504 Universal Trade Centre, 3 Arbuthnot Road, Central, Hong Kong SAR.
- **Service** refers to the Website.
- **You** means the individual accessing the Service, or the company, or other legal entity on behalf of which such individual is accessing or using the Service, as applicable.
- **Website** refers to Edutor Consulting, accessible from https://edutor.org

## 2. Disclaimer

The information contained on the Service is for general information purposes only.

The Company assumes no responsibility for errors or omissions in the contents of the Service.

In no event shall the Company be liable for any special, direct, indirect, consequential, or incidental damages or any damages whatsoever, whether in an action of contract, negligence or other tort, arising out of or in connection with the use of the Service or the contents of the Service. The Company reserves the right to make additions, deletions, or modifications to the contents on the Service at any time without prior notice.

The Company does not warrant that the Service is free of viruses or other harmful components.

## 3. Third Party Information

The Information may contain information provided by third parties or sourced by us from business information sources and other references or sources.

We are not responsible if any such Information is not up-to-date. We do not separately verify the Information provided by such third party or agency and you are cautioned thus to the reliability and accuracy of the Information accordingly. We do not accept any responsibility whatsoever in respect of any such Information. Any use of and reliance of such Information is at your own risk.

## 4. Prohibition of Access

Whilst we shall use commercially reasonable efforts to provide users with access to this Website twenty-four (24) hours per day, we cannot guarantee your access and you acknowledge that we make no representation or warranty that this Website will be available. You are responsible for your own hardware, software and facilities and we cannot guarantee compatibility of this Website with your systems.

We shall be entitled, at our sole discretion, to (a) temporarily suspend this Website to upgrade or modify this Website, and/or (b) restrict your access to and use of this Website where we consider it reasonably necessary for the operation of this Website. We shall not be responsible for any loss or damage you may incur as a result of any such suspension, restriction or prohibition.

We reserve the right at all times to prohibit or deny your access to this Website or any part thereof immediately and without notice, where we are of the opinion that you have breached any of the terms contained in these Terms of Use or that such prohibition or denial is appropriate, desirable or necessary in our sole opinion.

## 5. External Links Disclaimer

The Service may contain links to external websites that are not provided or maintained by or in any way affiliated with the Company.

Please note that the Company does not guarantee the accuracy, relevance, timeliness, or completeness of any information on these external websites.

Links to other websites do not constitute an endorsement or recommendation by the Company of such websites or the Information, products, advertising or other materials available on those websites.

## 6. Embedded Content

Embedded content (including embedded images, audio and video content and text) on this Website that is identified with social media such as Facebook, Twitter, YouTube, Sina Weibo and Instagram are embedded from third-party hosted sites. Embedded content is subject to the licence terms of the website or digital platform at which such content is hosted or the licence terms imposed by the owner of such content. Embedded content may be used or linked to only in accordance with the terms of the relevant licence. You are liable for any consequences of failure to comply with those licence terms.

## 7. Errors and Omissions Disclaimer

The information given by the Service is for general guidance on matters of interest only. Even if the Company takes every precaution to ensure that the content of the Service is both current and accurate, errors can occur. Plus, given the changing nature of laws, rules and regulations, there may be delays, omissions or inaccuracies in the information contained on the Service.

The Company is not responsible for any errors or omissions, or for the results obtained from the use of this information.

## 8. Fair Use Disclaimer

The Company may use copyrighted material which has not always been specifically authorized by the copyright owner. The Company is making such material available for criticism, comment, news reporting, teaching, scholarship, or research.

The Company believes this constitutes a "fair use" of any such copyrighted material as provided for in section 107 of the United States Copyright law.

If You wish to use copyrighted material from the Service for your own purposes that go beyond fair use, You must obtain permission from the copyright owner.

## 9. Views Expressed Disclaimer

The Service may contain views and opinions which are those of the authors and do not necessarily reflect the official policy or position of any other author, agency, organization, employer or company, including the Company.

Comments published by users are their sole responsibility and the users will take full responsibility, liability and blame for any libel or litigation that results from something written in or as a direct result of something written in a comment. The Company is not liable for any comment published by users and reserves the right to delete any comment for any reason whatsoever.

## 10. No Responsibility Disclaimer

The information on the Service is provided with the understanding that the Company is not herein engaged in rendering legal, accounting, tax, or other professional advice and services. As such, it should not be used as a substitute for consultation with professional accounting, tax, legal or other competent advisers.

In no event shall the Company or its suppliers be liable for any special, incidental, indirect, or consequential damages whatsoever arising out of or in connection with your access or use or inability to access or use the Service.

## 11. "Use at Your Own Risk" Disclaimer

All information in the Service is provided "as is", with no guarantee of completeness, accuracy, timeliness or of the results obtained from the use of this information, and without warranty of any kind, express or implied, including, but not limited to warranties of performance, merchantability and fitness for a particular purpose.

The Company will not be liable to You or anyone else for any decision made or action taken in reliance on the information given by the Service or for any consequential, special or similar damages, even if advised of the possibility of such damages.

## 12. Contact Us

If you have any questions about this Disclaimer, You can contact Us:

By email: edutorconsulting@gmail.com

By visiting this page on our website: https://edutor.org/contact