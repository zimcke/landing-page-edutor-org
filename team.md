---
title: Team
layout: teams
description: Team
permalink: "/team/"
intro_image_absolute: true
intro_image_hide_on_mobile: false
---

# Meet The Team

Our team of unique educational consultants will help you make the right decision and guide you through your Belgian adventure abroad.
