---
title: "Programme Selection"
date: 2019-02-28T15:15:34+10:00
weight: 3
---

- Assistance in choosing the right academic program based on your interests and career objectives.