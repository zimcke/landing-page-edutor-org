---
title: "University Selection"
date: 2018-11-18T12:33:46+10:00
weight: 1
---

- Personalized assessment of your academic and career goals.
- Research and recommendations for suitable universities in Belgium.