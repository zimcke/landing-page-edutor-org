---
title: "Admissions Assistance"
date: 2019-01-28T15:15:26+10:00
weight: 2
---

- Guidance on the application process for Belgian universities.
- Assistance with preparing and submitting application documents.