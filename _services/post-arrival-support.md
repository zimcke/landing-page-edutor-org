---
title: "Post-Arrival Support"
date: 2019-06-18T12:33:46+10:00
weight: 8
---

- Assistance with registration and orientation at your chosen university.
- Help securing local accommodation.
- Ongoing support during your stay in Belgium.