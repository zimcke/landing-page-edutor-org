---
title: "Visa Guidance"
date: 2018-12-28T15:14:39+10:00
weight: 4
---

- Information on visa requirements and procedures for studying in Belgium.
- Support in preparing and submitting visa applications.