---
title: Privacy Policy
layout: page
description: Privacy Policy
bodyClass: page-about
---

## 1. Introduction

This is the Privacy Policy of Edutor Consulting by BytePact Ltd. (2504 Universal Trade Centre, 3 Arbuthnot Road, Central, Hong Kong SAR),  (hereinafter: “Edutor Consulting”, “we” or “us”). The proper handling of personal data is extremely important to us. We therefore take great care when processing and protecting your personal data. This is done in accordance with provisions set out in the applicable privacy laws (including the General Data Protection Regulation).

## 2. Purpose of this privacy policy

This Privacy Policy applies to the website https://edutor.org and to all (commercial) relationships between Edutor Consulting and its customers, prospects and business partners. This Privacy Policy relates to marketing, including visiting the website or other applications, the organisation of business, the content or marketing events, all forms of information provision via newsletters, articles on the website, webinars, online consultations or via social media, and (customer) relationship management.

This Privacy Policy describes, among other things, the types of personal data we collect about you, where we obtain the data from, how we use the data we collect about you, how long we retain your data, to whom we may provide this information, and your rights with regard to the processing of your personal data.

## 3. Our core values 

To ensure your privacy as far as possible, we adhere to the following core values:

- **Information**: We wish to inform you about why and how we use your personal data. This is described in this Privacy Policy.
- **Limited collection**: We take great care to limit the information we collect and use to only that which is necessary for providing our Services (as defined below).
- **Security**: We take the appropriate measures to secure your personal data and require the same from the parties that use your data on our behalf. Edutor Consulting proactively tests on a continuous basis its websites and apps by means of an automated security scan.
- **Individuals’ rights**: We respect your rights under applicable privacy laws (including the right to access, correct, or delete your personal data in accordance with such laws). This Privacy Policy describes how we implement these core values and how we respect and protect your right to privacy. We strongly urge you to read this statement carefully, as it sets out the basis on which we use your personal data when you access our websites, interact with us or otherwise use our services and applications (jointly referred to as: “Services”). Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it.

## 4. Personal data we collect and use 

The personal data we collect about you when you buy our Products or use our Services, include the following (hereinafter jointly referred to as: “Personal Data”):

- **Your identification details.** First name and surname, gender, date of birth, etc.
- **Your contact details.** Your address and other contact details, such as your telephone number and e-mail address, and any other contact details you provide to us.
- **Your educational interests.** Which services or products from Edutor Consulting are or could be of interest to you. The preferences that are shown from your visit to our websites or applications, as well as preferences or interests that are expressed by participation in business, content or marketing events, but, of course, also from interests that you yourself have indicated or that we determine on the basis of professional social media use.
- **Your electronic identification data and other information we automatically collect when using our online Services.** When visiting our websites or apps, interfere (opening, clicking, reading, cursor-movement etc.) with our newsletter or being confronted with online marketing advertisements of EdTech Hong Kong, we collect data automatically through your browser or device, by making use of cookies and other technologies to track visitors on the website (including web analytics), such as your IP address, MAC address, your browsing behaviour and the Services you like. Please see our Cookie Policy to find out more.
- **Your account data.** To complete your account at Edutor Consulting (if applicable, e.g. our Support Centre) (online or in-person via the onboarding application), we use your name, gender, email address, date of birth (plus social security number if this is linked to your birthday under local law) and other information you voluntarily provide to us, such as your telephone number, additional interests or personal product advice (such as but not limited to study domains and programmes, universities).
- **Your purchases online.** To complete your online purchases via the official Edutor Consulting website or via other official social media-channels of Edutor Consulting, we use your name, gender, email address, telephone number (if you voluntarily provide this to us) and all relevant information regarding your purchases, such as the amount and date of your purchases, the Product(s) or Service(s) you purchase, the device through which you make your purchases, payment method, payment status, bank account details,  and voucher code used (if any). In order to offer you third-party partner payment methods, we might pass your personal data in the form of contact and order details to said third parties in the checkout process, in order for them to assess whether you qualify for their payment methods and to tailor those payment methods for you.
- **Your data when subscribing to our newsletter.** To be able to provide you with the newsletter of Edutor Consulting containing inspirational and commercial content, we use your email address and additional information if you add such to your profile, such as name, date of birth and your personal background. Edutor Consulting also sends out personalized newsletters, based on your personal preferences if you’ve provided us with those. Your communication data. Any data shared by you when communicating with us via email, online, telephone, social media or any other form, such as questions, requests or complaints.
- **Recordings (webinars, events and telephone recordings).** If you attend webinars, events or fairs that Edutor consulting hosts or attends, you can also be recorded if we make video footage of such webinar, event or fair. If you call us with a question or complaint, your telephone calls to us can be recorded.
- **Your communication data.** Any data shared by you when communicating with us via email, online, telephone, social media or any other form, such as questions, requests or complaints.
- **Your ratings and reviews and feedback.** The opinions, experiences, preferences and interests and event reviews that you publish on our websites or share with us online or through social media.

In principle, we do not process sensitive personal data about you. If this data is actually processed, this is purely for the purpose of general data processing and never with the intention of using this particular data. In principle, we only process personal data that are relevant in a professional context.

## 5. Purposes of use of personal data

The Personal Data we collect is exclusively used for the following purposes:

- **For the performance of our agreement with you**: In order to carry out our obligations arising from any contracts entered into between you and us, and to provide you with the Services and information that you request, including managing and handling your requests, inquiries or complaints. This also includes enabling you to participate in our Edutor Consulting program, to responding to your requests to provide customer service, respond to your inquiries, provide you with essential information regarding our Services you request, etc.
- **The request for information or an offer**: In order to offer you personal information or a custom offer based on your interests and personal educational background.
- **For our legitimate commercial interests**: We use your Personal Data as described above (both on aggregated and on individual basis) for the purpose of advertising our Services, to contact you via e-mail, regular mail, social media or otherwise for direct marketing or other commercial purposes. We also use your Personal Data for analysing and improving the quality of our Services, such as providing you with customer services and aftersales, and to understand you as a customer (customer optimalization). This enables us to assess what may interest you, to measure or understand the effectiveness of advertising we serve to you and others and to deliver relevant advertising. In addition, based on your use of our Services, we may target you with advertisement or other marketing materials that are customized to your personal preferences and experiences. We may also use your Personal Data, for our other legitimate commercial interests such as to operate and expand our business activities; to develop and improve or modify our Services; to generate aggregated statistics about the users of our Services; to facilitate our business operations; to operate company policies and procedures; to enable us to make corporate transactions, such as any merger, sale, reorganization, transfer of Edutor Consulting’s assets or businesses, acquisition, bankruptcy, or similar event; or for other legitimate business purposes permitted by applicable law.
- **Use of information based on your consent**: If you haven’t purchased any Service from us, we will only send you direct marketing communications (such as newsletters, promotions, news on products or service updates) via email, other electronic means, via telephone or via hardcopy mail (such as flyers), after we have received your consent to do so. Children under the age of 16 cannot legally give their consent. Instead, consent of their parent or legal guardian needs to be provided. You can withdraw your consent at any time (see the section Your rights below).
- **To comply with our legal obligations**: Any Personal Data we collect may be used to comply with a legal obligation to which we are subject, such as supervisory bodies, fiscal authorities or investigation bodies.

## 6. Social media

When you participate in various social media forums like Facebook, Twitter, Pinterest, Instagram, LinkedIn, etc., you should be familiar with and understand the tools provided by those sites that allow you to make choices about how you share the personal data in your social media profile(s). Edutor Consulting is bound by the privacy practices or policies of these third parties, so we encourage you to read the applicable privacy notices, terms of use and related information about how your personal data is used in these social media environments.

## 7. Sharing your personal data

We share your Personal Data with the following parties:

- **Third Party suppliers.** We engage third parties, from time to time, to help us providing us our Services, including:
    - Business partners, suppliers (such as IT service providers) and sub-contractors;
    - Advertising and media companies that carry out marketing and media activities on our behalf; 
    - Analytics and search engine providers that assist us in the improvement and optimization of our website and apps, such as Google Analytics.

  In providing their services, they will access, receive, maintain or otherwise process Personal Data on our behalf. Our contracts with these service providers do not permit use of your Personal Data for their own (marketing) purposes. Consistent with applicable legal requirements, we take commercially reasonable steps to require such Third Party suppliers to adequately safeguard your Personal Data and only process it in accordance with our instructions.

- **Corporate transaction.** In addition, Personal Data may be disclosed as part of any merger, sale or transfer of Edutor Consulting’s assets.
- **Third parties in case of legal requirement.** We may also disclose your Personal Data if we believe we are required to do so by law.
- **With consent.** We may also disclose information about you, including Personal Data to any other third party, where you have consented or requested that we do so, for example in case of a promotional campaign with another sponsor.

## 8. International transfers of personal data

In most cases your Personal Data will be processed within the Hong Kong SAR and Belgium. However, please be informed that Edutor Consulting may transfer and process any Personal Data you provide to us to countries other than your country of residence. The laws of these countries may not provide the same level of protection to your Personal Data. Edutor Consulting or Third Party suppliers we use will therefore seek to ensure that all adequate safeguards are in place and that all applicable laws and regulations are complied with in connection with such transfer. This means that we entered into legally necessary contracts with recipients of your data, including standard contractual clauses as approved by supervisory authorities where required. You are entitled to receive a copy of any documentation showing the suitable safeguards that have been taken by making a request via edutor-consulting@gmail.com.

## 9. Security

We will take reasonable steps to ensure that your Personal Data are properly secured using appropriate technical, physical, and organizational measures, so that they are protected against unauthorised or unlawful use, alteration, unauthorised access or disclosure, accidental or wrongful destruction, and loss.

We take steps to limit access to your Personal Data to those persons who need to have access to it for one of the purposes listed in this Privacy Policy. Furthermore, we contractually ensure that any Third Party supplier processing your Personal Data equally provide for confidentiality and integrity of your data in a secure way.

## 10. Data retention

We retain your Personal Data for as long as required to satisfy the purpose for which they were collected and used (for example, for the time necessary for us to provide you with customer service, answer queries or resolve technical problems), unless a longer period is necessary for our legal obligations or to defend a legal claim.

## 11. Your rights

Subject to the conditions set forth in the applicable law, you have the the following rights with regard to our processing of your Personal Data:

**Right of access** – You have the right to request confirmation if Edutor Consulting processes personal data about you, and if such is the case, access to the personal data and additional information. Upon request, we can also provide you with a copy of the personal data undergoing processing;

**Right to rectification** – You have the right to request that Edutor Consulting corrects, adjusts or completes your Personal Data if we have inaccurate or incomplete data relating to you. We also kindly request you to ensure that changes in personal circumstances (for example, change of address, etc.) are notified to Edutor Consulting so that we can ensure that your Personal Data is up-to-date. Edutor Consulting will take all reasonable steps to ensure that all Personal Data are correct;

**Right to withdraw consent** – You have the right to revoke your consent for receiving marketing communications at any time, by following the instructions in any marketing communication or by filling in the form as set out below. If we use your Personal Data based on your consent for other reasons than marketing communication, you can revoke your consent by filling the form as set out below;

**Right to delete** - You have the right to request deletion of any irrelevant Personal Data we hold about you when (i) the data is no longer necessary in relation to the purposes for which they are collected; (ii) you withdraw your consent and there is no other legal ground for processing; (iii) you object to the processing in case of direct marketing purposes, or – in any other case - there is no overriding legitimate ground for processing; (iv) we unlawfully processed your data. If you have such a request and all requirements are met, we shall make sure that Edutor Consulting erases the data of which we are not under a legal obligation to retain;

**Right to restriction of data use** - You have the right to restrict our use of your Personal Data where (i) you contest the accuracy of the Personal Data; (ii) the use is unlawful but you do not want us to erase the data; (iii) we no longer need the Personal Data for the relevant purposes, but you require them for the establishment, exercise or defence of legal claims; or (iv) you have objected to data use justified on our legitimate interests pending verification as to whether Edutor Consulting has indeed compelling interests to continue the relevant data use;

**Right to data portability** - to the extent that we use your Personal Data for the performance of an agreement with you, and that personal data is processed by automatic means, you have the right to receive all such Personal Data which you have provided to Edutor Consulting in a structured, commonly used and machine-readable format, and also to require us to transmit it to another data controller where this is technically feasible;

**Right to object** - to the extent that we are relying on our legitimate interests to use your Personal Data, you have the right to object to such use, and we must stop such processing unless we can either demonstrate compelling legitimate grounds for the use that override your interests, rights and freedoms or where we need to process the data for the establishment, exercise or defence of legal claims. You also have the right to object to profiling activities conducted by Edutor Consulting;

**Right to lodge a complaint** - You also have the right to lodge a complaint with a supervisory authority, in particular in your Member State of residence, if you consider that the collection and use of your Personal Data infringes this Privacy Policy or applicable law.

To exercise any of your rights, please make a request via edutor-consulting@gmail.com. Upon receipt of your email, we shall respond to your request consistent with applicable law.

## 12. Changes to the policy

This Privacy Policy may be revised from time to time. If a fundamental change to the nature of the use of your personal data is involved or if the change is in any other manner relevant to you, we will ensure that information is provided to you well in advance of the change actually taking effect.

## 13. Contact us

If you have any queries about this Privacy Policy or our handling of your Personal Data in general, please email us at edutorconsulting@gmail.com or contact us at https://edutor.org/contact and be sure to indicate the nature of your query.