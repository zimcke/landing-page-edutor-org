---
title: Edutor Consulting | Guide to Studying Abroad in Belgium
layout: home
description: At Edutor Consulting, we believe that Belgium is a hidden gem in the world of international education.
intro_image: "images/illustrations/pointing.svg"
intro_image_absolute: true
intro_image_hide_on_mobile: true
show_call_box: true
---

# Studying Abroad in Belgium

## Access to top 100 universities

We offer students easy and affordable access to top-notch Belgian universities for a successful future. We can guide you through the application process and help you secure local accomodation.
