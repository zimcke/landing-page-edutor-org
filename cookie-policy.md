---
title: Cookie Policy
layout: page
description: Cookie Policy
bodyClass: page-about
---

The following Cookie Policy shall apply for Edutor Consulting by BytePact Ltd. (2504 Universal Trade Centre, 3 Arbuthnot Road, Central, Hong Kong SAR) and using the website https://edutor.org.

This cookie policy shall apply to all web pages of Edutor Consulting (starting with https://edutor.org).

Edutor Consulting employs cookies to make your surfing experience easier and more user-friendly, and to tailor the content of its website to your specific requirements and preferences.

We ask for your consent for the different types of cookies using a pop-up window during your (first) visit to our website. You can always change this afterwards via your browser settings. If you click on 'Accept' you are giving your specific consent for the use of any relevant cookies in accordance with this statement. You can specify your consent per type of cookie. You can withdraw your consent at any time, where applicable per type of cookie, by changing your cookie settings (see below).

## 1. What are cookies?
Cookies are small text files that can be used by websites to facilitate their functioning, to make user experiences more efficient or to provide information to the owner of the site.

These text files are stored on your device when you visit our website. They can be deleted at any time via your browser settings.

## 2. What is the legal basis for cookies?
   Cookies are collated and processed on the basis of

- your consent for non-essential cookies;
- your legitimate interest in essential cookies.

## 3. Which cookies does Edutor Consulting collect and for what purpose?

Edutor Consulting uses different cookies, classified according to their functionality. We only use these cookies for the purposes explained below and not for any other purposes that are incompatible with the original purpose. We have listed them in tables to provide maximum transparency with respect to the cookies we use.

However, despite all the precautions we have taken to create a complete list of cookies used, some cookies may be replaced by others that are not listed in the tables below. That is why this policy may occasionally be changed (see below).

### Necessary
Necessary cookies make a website more user-friendly by facilitating basic functions such as page navigation and access to secure areas of the website. Without these cookies, the website would not function properly.

| Name | Provider          | Purpose | Expiry | Type      |
| ---- |-------------------| ------- | ------ |-----------|
| ajs_anonymous_id | www.atlassian.com |         |        |           |
| __aid_user_id | www.atlassian.com |         |        |           |
| optimizelyEndUserId | www.atlassian.com |         |        |           |
| atlCohort | www.atlassian.com |         |        |           |
| atlUserHash | www.atlassian.com |         |        |           |
|      |                   |         |        |           |



### Functional
Functional cookies make your visit to our website easier and more user-friendly. For example, these cookies ensure that a website or app remembers certain parameters, such as your preferences in terms of language, layout and colours. We use functional cookies to enhance the user-friendliness of our website.

| Name | Provider | Purpose | Expiry | Type      |
| ---- |----------| ------- | ------ |-----------|
|      |          |         |        |           |

### Performance
We use performance cookies to collect information about how internet users use our website (pages visited, average duration of a visit, etc.). This enables us to improve the overall operation of our website.

| Name | Provider                 | Purpose | Expiry  | Type          |
|------|--------------------------| ------- |---------|---------------|
| td   | www.googletagmanager.com | Registers statistical data on users' behaviour on the website. Used for internal analytics by the website operator. | Session | Pixel tracker |

### Marketing
Marketing cookies are targeting or advertising cookies, which are used to collect information about users' browsing behaviour. Their purpose is to provide relevant content or advertisements that may interest you as a user of our website.

| Name | Provider | Purpose | Expiry | Type      |
| ---- |----------| ------- | ------ |-----------|
|      |          |         |        |           |

### Third party cookies
Some of our pages show content from external providers. To view third-party content, you must first accept their specific terms and conditions, including their cookie policy, which is outside our control.

If you do not view this content, no third-party cookies will be placed on your device. If these third-party cookies are disabled, communication problems may occur with the Edutor Cosulting website.

For further information on how these third parties manage cookies, please refer to their relevant statements:

- Google
- LinkedIn
- Facebook
- Twitter

Please note: The statements of the third parties may change regularly. They may therefore change their conditions of use, the purpose and use of cookies, etc. at any time. Edutor Consulting has no control over this.

You can also delete cookies already installed on your computer or mobile device at any time, also via your web browser.

- Cookies in Internet Explorer
- Cookies in Mozilla Firefox
- Cookies in Google Chrome
- Cookies in Apple Safari

## 4. Allow or block cookies?

Most web browsers are programmed to accept cookies automatically. However, you can configure your browser to accept or block specific cookies.

Please note, however, that many websites will not function optimally if cookies are disabled, although this has no impact on the services provided.

## 5. Changes to this cookie policy

We will periodically review and amend this statement if necessary. The most recent version of our cookie policy will always be posted on our website.

Before a potential change takes effect, your consent will be requested - if necessary - by means of a pop-up window on our website during your next visit.

## 6. Contact

Should you have any further questions, please do not hesitate to contact Edutor Consulting by sending an e-mail to edutorconsulting@gmail.com.