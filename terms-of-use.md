---
title: Terms of Use
layout: page
description: Terms of Use
bodyClass: page-about
---

UNLESS OTHERWISE SPECIFIED BY EDUTOR CONSULTING ("we", "us" or "our") IN THIS WEBSITE OR PURSUANT TO SEPARATE LICENCES GRANTED BY US, YOU MUST NOT DOWNLOAD, COPY, REPRODUCE, PUBLISH, TRANSMIT, DISTRIBUTE OR RESELL ANY PICTURES, IMAGES OR CONTENT FROM THIS WEBSITE IN ANY FORMAT AND BY ANY MEANS. FOR REQUISITION OF OUR AGREEMENT/ LICENCE FOR USE OF OUR PICTURES, IMAGES OR CONTENT, PLEASE REFER TO edutorconsulting@gmail.com FOR ENQUIRY.

BY USING THIS WEBSITE, YOU ARE BOUND BY THE TERMS OF USE STATED HEREIN.

## 1. Information

This website, including the set of related web pages located under our domain name https://edutor.org ("Website"), presents information, data, content, photo, picture, video, audio and other materials and items, tangible or intangible (collectively, the “Information”). By visiting this Website, you agree that such access is your understanding and agreement that we are granting you a non-exclusive, non-transferable and limited licence to access this Website in accordance with these Terms of Use.

## 2. Change of Terms of Use

We may change, add to or delete terms from these Terms of Use from time to time in our sole discretion without notice or liability to you. By continuing to use this Website following such modifications to the Terms of Use, you agree to be bound by such modifications.

## 3. Changes to Website

We may, at our absolute discretion and at any time, without prior notice to you, add to, amend or remove material from this Website, or alter the presentation, substance, or functionality of this Website.

## 4. User Conduct on this Website

As a condition of your use of this Website, you may not:

| (a) | trespass, break into, access, use or attempt to trespass, break into, access or use any other parts of our servers, and/or any data areas for which you have not been authorized by us; |
| (b) | restrict or inhibit any other user from using and enjoying this Website; |
| (c) | post or transmit any unlawful, fraudulent, libelous, defamatory, obscene, pornographic, profane, threatening, abusive, hateful, offensive, or otherwise objectionable or unreasonable information of any kind, including without limitation any transmissions constituting or encouraging conduct that would constitute a criminal offence, give rise to civil liability, or otherwise violate any local, national or foreign law, infringe any intellectual property right, proprietary rights or confidentiality obligations of others; |
| (d) | post or transmit any advertisements, solicitations, chain letters, pyramid schemes, investment opportunities or schemes or other unsolicited commercial communication, or engage in spamming or flooding; |
| (e) | post or transmit any information or software which contains a virus, trojan horse, worm, spyware or other harmful component; |
| (f) | post, publish, transmit, reproduce, distribute or in any way exploit any Information obtained through this Website for commercial purposes without our prior written permission (unless otherwise specified by us in this Website or pursuant to separate licence granted by us); |
| (g) | upload, post, publish, transmit, reproduce, or distribute in any way, any component of this Website itself or any Information obtained through this Website which is protected by copyright, or other proprietary right, or create derivative works with respect thereto, without our prior written permission (unless otherwise specified by us in this Website or pursuant to separate licence granted by us); |

You have no rights in or to the Information and you will not use the Information, except as permitted under these Terms of Use and/or in accordance with separate licence granted by us.

## 5. User-Generated Content

This Website may include features that support and publish User-Generated Content, which is defined as any comments, discussions, Visualization Threads, audio and video content and other content submitted by registered users for uploading onto the Website. By accessing the Website, you agree that you will not make use of these features:

| (a) | To make comments that are threatening, knowingly false, or unlawful; to use foul language; or to engage in personal attacks;  |
| (b) | To impersonate any person or entity or create a false identity (other than a pseudonym) on the Website; |
| (c) | To post material that infringes a copyright, trademark or patent right, trade secret or other legal right of any person, corporation or institution or any right of privacy; |
| (d) | To collect, print out, reproduce, distribute, or otherwise make available or use any personally-identifiable information about other users; |
| (e) | To engage in any conduct prohibited under these Terms of Use. |

By submitting User Generated Content for publication on this Website, you grant Edutor Consulting an irrevocable, worldwide, transferable, royalty-free, non-exclusive, sub-licensable license to reproduce, publicly perform, publicly display, edit, create derivative works of, and distribute said content.

By submitting User-Generated Content, you warrant and represent that you own or otherwise control all of the rights to your content, including without limitation, all the rights necessary for you to provide, post, upload, input or submit the content, grant the license as abovementioned and for us to post, upload, cross-post and cross-upload the content.

You may edit or remove content you have posted on this Website at any time. When you delete content from the Website, such deleted content, while not available to the viewing public and other site users, will remain on the Website server until such time as you make a specific request to us for permanent deletion of such content from the Website server. (We may retain an archival copy of such content as may be required by relevant law, rule or regulations or for auditing purposes.) Such requests may be made in writing, via email as follows:

Email to: edutorconsulting@gmail.com

You further understand and agree that User-Generated Content posted to the Website will be made available for the public to access, view, and use under the License described above. Specifically, you understand that if you submit a Visualization Thread for publication on the Website, activate the Website functionality or respond affirmatively to the Website messages and enable public sharing, it will be visible to anyone in the public.

You understand and agree that we may monitor User-Generated Content on the Website, and that we reserve the right to modify, edit, or remove any of said content at our discretion, without notice, and for any reason. You further understand and agree that we may prescreen User-Generated Content and may decide, in our discretion, without notice, and for any reason, not to publish it. We assume no responsibility for monitoring, modifying, removing, or declining to publish User-Generated Content.

You further understand and agree that the views and opinions expressed by other users on this Website are theirs alone and should not be ascribed to Edutor Consulting. User-Generated Content and Third Party Information are the sole responsibility of the users and third parties, and their accuracy and completeness are not endorsed or guaranteed by us. User-Generated Content and Third Party Information are the property of the creator or copyright holder and we claim no right, title or interest to any such content and disclaim all warranties and liabilities related thereto.

If you believe your copyright-protected work was posted on this Website without authorization, you may submit a copyright infringement notification to us requesting for removal of the content, via email as follows:

Email to: edutorconsulting@gmail.com

These requests should only be submitted by the copyright owner or an agent authorized to act on the owner’s behalf together with valid proof of ownership. We reserve the right to modify, edit, or remove any of said content subject to copyright infringement notification at our discretion, without notice, and for any reason. You agree that we may terminate your account if your account has been notified of infringing activity more than twice.


## 6. Disclaimer

For information about our disclaimer, please refer to our Disclaimer.

## 7. Intellectual Property Right

All intellectual property rights subsisting in respect of this Website belong to us or have been lawfully licensed to us for use on this Website. All rights under applicable laws are hereby reserved. While you may download or print material from this Website for your personal, non-commercial use, it remains the property of Edutor Consulting, unless otherwise stipulated.

Unless otherwise specified by us in this Website or pursuant to a separate licence granted by us, you are not allowed to upload, download, post, publish, reproduce, transmit or distribute in any way any component of this Website itself or create derivative works with respect thereto as this Website is copyrighted under applicable laws.

You agree that we are free to use, disclose, adopt and modify all and any ideas, concepts, know-how, proposals, suggestions, comments and other communications and information provided by you to us (“Feedback”) in connection with this Website without any payment to you. You hereby waive and agree to waive all and any rights and claims for any consideration, fees, royalties, charges and/or other payments in relation to our use, disclosure, adoption and/or modification of any or all of your Feedback.

## 8. Limited Liability and Warranty

All Information is for your general reference or use pursuant to any licence separately granted by us only. We do not accept any responsibility whatsoever in respect of such Information.

YOUR ACCESS TO AND USE OF OUR WEBSITE IS AT YOUR SOLE RISK AND IS PROVIDED “AS IS”, “AS AVAILABLE”. THIS WEBSITE IS FOR YOUR PERSONAL USE ONLY AND WE MAKE NO REPRESENTATION OR WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, ANY PRODUCTS ADVERTISED, BOUGHT OR SOLD USING THIS WEBSITE OR ANY WARRANTIES ON MERCHANTABILITY OR FITNESS FOR ANY PARTICULAR PURPOSE. REFERENCE IN THIS WEBSITE TO ANY THIRD PARTY PRODUCTS, EVENTS OR SERVICES DO NOT CONSTITUTE OR IMPLY OUR ENDORSEMENT OR RECOMMENDATION OF ANY KIND.

Without limiting any exclusion specifically provided for in these Terms of Use, we shall in no event be liable for any costs, damages or liability for any unauthorized use of this Website or breach of security relating to this Website.

We do not guarantee or assume any responsibility that, and you acknowledge that we make no representation or warranty, that:

| (a) | the Information on this Website is accurate, adequate, current or reliable, or may be used for any purpose other than for general reference or as specified in any separate licence granted by us; |
| (b) | the Information on this Website is free of defect, error, omission, virus or anything which may change, erase, add to or damage your software, data or equipment; |
| (c) | messages sent through the internet will be free from interception, corruption or loss; |
| (d) | access to this Website will be available or be uninterrupted; or |
| (e) | defects in this Website will be corrected. |

In no event shall we be liable (whether in tort or contract or otherwise) to you or any other person for any direct, indirect, incidental, special, punitive or consequential damages, including any loss of business or profit, arising out of any use, or inability to use, errors or omission in this Website or the Information, even if we have been advised of the possibility of such damages.

You will exercise and rely solely on your own skill and judgment in your use of this Website and use and interpretation of the Information. You are responsible for ensuring that your use of this Website and the Information complies with all applicable legal requirements.

The limitation of liability contained in these Terms of Use will apply to the fullest extent permitted by applicable laws.

## 9. Indemnity

You agree to defend, indemnify and hold us, our employees, agents, officers, directors, agents, contractors, suppliers and other representatives harmless from and against all liabilities, damages, claims, actions, costs and expenses (including legal fees), in connection with or arising from your breach of these Terms of Use and/or your use of this Website. We may, if necessary, participate in the defence of any claim or action and any negotiations for settlement. You will not make any settlement that may adversely affect our rights or obligations without our prior written approval. We reserve the right, at our own expense and on notice to you, to assume exclusive defence and control of any claim or action.

## 10. Privacy Policy

For information about our privacy policies and practices, please refer to our Privacy Policy.

## 11. Partial Invalidity

The illegality, invalidity or unenforceability of any provisions of these Terms of Use under the law of any jurisdiction shall not affect its legality, validity or enforceability under the laws of any other jurisdiction nor the legality, validity or enforceability of any other provision.

## 12. Miscellaneous

No waiver of any breach under these Terms of Use will amount to a waiver of any other breach. The headings in these Terms of Use are for convenience only and do not affect interpretation.

## 13. Governing Law and Jurisdiction

These Terms of Use shall be governed by the law of the Hong Kong Special Administrative Region of the People’s Republic of China. You agree to submit to the exclusive jurisdiction of the Hong Kong Court.