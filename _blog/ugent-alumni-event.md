---
title: "UGent Alumni Networking Event in Hong Kong "
date: 2024-05-23
image: "images/blog/ugent-alumni-event.jpg"
image-caption: "UGent (Ghent University) is holding an alumni networking event in Hong Kong on 6 June 2024 at 6:30pm."
tag: "Universities"
---

UGent (Ghent University) is holding an alumni networking event in Hong Kong on 6 June 2024 in the presence of the university Rector. Are you a Belgian or foreign student who studied at Ghent University? Join this amazing networking opportunity: <a href="https://event.ugent.be/registration/AlumniHongKong" target="_blank">https://event.ugent.be/registration/AlumniHongKong</a>
